'use strict';

/* Services */
var iescitiesAppServices = angular.module('iescitiesAppServices', []);


/* Helper variables */
var retryCounterA = 0;
var retryCounterB = 0;


/** Log Service */
iescitiesAppServices
		.factory(
				'iesCitiesService',
				[
						'$http',
						'$rootScope',
						function($http, $rootScope) {
							return {
								// Iescities server app log
								sendLog : function(message, type) {
									var uri = "http://150.241.239.65:8080/IESCities/api/log/app/stamp/";
									var timestamp = Math.floor(new Date().getTime() / 1000);
									var appid = "YouDecide";
									var session = $rootScope.sessionId;

									uri = uri + timestamp + "/" + appid + "/"
											+ session + "/" + type + "/"
											+ message;
									return $http(
											{
												method : 'GET',
												url : uri,
												headers : {
													'Accept' : 'application/json',
													'Content-Type' : 'application/json;charset=UTF-8'
												}
											})
											.success(
													function(data) {
														console.log("iesCitiesService.sendLog() - SUCCESS");
														console.log(JSON.stringify(data));
													})
											.error(
													function(data) {
														console.log("iesCitiesService.sendLog() - ERROR");
														console.log(JSON.stringify(data));
													});
								}

							};
						} ]);

/** Bristol Voting Service, uses IES Cities API */
iescitiesAppServices.factory('votingService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {
				// GET: get Categories Static
				getCategories : function(callback) {					
						return $http.get('scripts/iescities/data/getCategories.json').success(callback);
				},
				// Get All propuestas (BRISTOL VERSION)
				getPropuestas : function(callback) {
					console.log("votingService.getPropuestas()");
					var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					var uri = "https://iescities.com:443/IESCities/api/data/query/328/sql?origin=original";
					var MySQLquery = "SELECT * FROM proposals";
					var starttime = (new Date).getTime();
					$http({
						method : 'POST',
						url : uri,
						timeout: 15000,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain'
						},
						data: MySQLquery
					}).success(function(data) {
						retryCounterA = 0;
						toastr.clear($(x));
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for getPropuestas: "+ duration+"ms");
						for (var i=0;i<data.rows.length;i++){  //Need to convert number of votes from string to int (IES API always returns strings)
							data.rows[i].votosAFavor = parseInt(data.rows[i].votosAFavor);
							data.rows[i].votosEnContra = parseInt(data.rows[i].votosEnContra);
							data.rows[i].start = new Date(data.rows[i].start); //Need to convert to JS date object for angularJS filter to work
							data.rows[i].end = new Date(data.rows[i].end);
							data.rows[i].startAportacion = new Date(data.rows[i].startAportacion);
							data.rows[i].endAportacion = new Date(data.rows[i].endAportacion);
							data.rows[i].creationDate = new Date(data.rows[i].creationDate);
						}
						console.log("getPropuestas() - OK");
						callback(data);
					}).error(function(data,status) {
						toastr.clear($(x));
						console.log("getPropuestas() - ERROR:"+JSON.stringify(data)+", status: "+status);						
						if (retryCounterA < 2){  // retry 2 times if original attempt fails
							retryCounterA = retryCounterA + 1;
							$rootScope.updatePolls();
						}
						else {
							toastr.error('Unable to retrieve polls. Please try again later.', 'Network error', {timeOut: 50000, closeButton : true , onHidden: function() {
								navigator.app.exitApp()
						}});
							console.log(JSON.stringify("URL: "+uri+" ------  Query: "+MySQLquery+" ------ Response: "+data));
							//callback(data);
						}
					});
				},
				
				// Get All propuestas with votes (STEF: I think it's complete now...)
				getPropuestasConVoto : function(accountId,callback) {
					console.log("votingService.getPropuestasConVoto()");
					var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					var starttime = (new Date).getTime();
					var uri = "https://iescities.com:443/IESCities/api/data/query/328/sql?origin=original";
					/* Left join to get back the user votes as well */
					var MySQLquery = 'SELECT distinct(proposals.id), proposals.title, proposals.type, proposals.category, ' +
							'proposals.description, proposals.aportaciones, proposals.votosAFavor, proposals.votosEnContra, ' +
							'proposals.start, proposals.end, proposals.startAportacion, proposals.endAportacion, ' +
							'proposals.creationDate, proposals.userID, votes.vote FROM proposals ' +
							'LEFT JOIN votes ON proposals.id=votes.proposalID AND votes.userID="'+accountId+'"';
					$http({
						method : 'POST',
						url : uri,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain'
						},
						data: MySQLquery
					}).success(function(data) {						
						console.log("getPropuestasConVoto() - OK");		
						/* Post-process results to change key name to spanish and delete empty 'vote' keys */
						for (var i=0;i<data.rows.length;i++){	
							data.rows[i].start = new Date(data.rows[i].start); //Need to convert to JS date object for angularJS filter to work
							data.rows[i].end = new Date(data.rows[i].end);
							data.rows[i].startAportacion = new Date(data.rows[i].startAportacion);
							data.rows[i].endAportacion = new Date(data.rows[i].endAportacion);
							data.rows[i].creationDate = new Date(data.rows[i].creationDate);
							if (data.rows[i].vote==='positivo'||data.rows[i].vote==='negativo'){
								data.rows[i].votoUsuario = data.rows[i].vote;
								delete data.rows[i].vote;
							}
							else {delete data.rows[i].vote;}
						}
						
			
						/* Get comments the user has made (for all proposals) */
						$http({
							method : 'POST',
							url : uri,
							headers : {
								'Accept' : 'application/json',
								'Content-Type' : 'text/plain'
							},
							data: 'SELECT * FROM comments WHERE userID="'+accountId+'";'
						}).success(function(data2) {
							var duration = (new Date).getTime() - starttime;
					    	console.log("==> Time for getPropuestasConVoto: "+ duration+"ms");
					    	toastr.clear($(x));
							var obj = new Array();
							for (var i=0; i<data2.rows.length;i++){
								var tempDate = new Date(data2.rows[i].creationDate);
								var temp = { 
									    "id" : parseInt(data2.rows[i].id),
									    "proposalID" : parseInt(data2.rows[i].proposalID),
									    "comment" : data2.rows[i].comment,
									    "creationDate" : tempDate,
									};
								obj.push(temp);
							}
							
							for (var i=0;i<data.rows.length;i++){
								data.rows[i].aportacionUsuario = new Array();
								var empty = true;
								for (var j=0;j<data2.rows.length;j++){
									if (parseInt(data.rows[i].id)==parseInt(data2.rows[j].proposalID)){
										data.rows[i].aportacionUsuario.push(data2.rows[j]); 
										empty = false;
									}
								}
								if (empty==true){
									delete data.rows[i].aportacionUsuario;
								}
							}
						}).error(function(data2,status) {
							toastr.clear($(x));
							alert("getPropuestas() - ERROR:"+JSON.stringify(data666));
						});								
						
						for (var i=0;i<data.rows.length;i++){  //Need to convert number of votes from string to int (IES API always returns strings)
							data.rows[i].votosAFavor = parseInt(data.rows[i].votosAFavor);
							data.rows[i].votosEnContra = parseInt(data.rows[i].votosEnContra);
						}
						callback(data);
					}).error(function(data) {
						toastr.clear($(x));
						console.log("getPropuestasConVoto() - ERROR");
						callback(data);
					});

				},	
				
				// Gets User's polls, including the user's votes on them
				getPropuestasDeUsuario : function(accountId,callback) {
					console.log("getPropuestasDeUsuario "+accountId);
					var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					var MySQLquery = 'SELECT DISTINCT(proposals.id), proposals.title, proposals.type, proposals.category, '+
					'proposals.description, proposals.aportaciones, proposals.votosAFavor, proposals.votosEnContra, '+
					'proposals.start, proposals.end, proposals.startAportacion, proposals.endAportacion, '+
					'proposals.creationDate, votes.vote FROM proposals LEFT JOIN votes ON '+
					'proposals.id=votes.proposalID AND proposals.userID="'+accountId+'" WHERE proposals.userID="'+accountId+'"';
					var uri = "https://iescities.com:443/IESCities/api/data/query/328/sql?origin=original";
					var starttime = (new Date).getTime();
					$http({
						method : 'POST',
						url : uri,
						timeout: 15000,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain'
						},
						data: MySQLquery
					}).success(function(data) {
						retryCounterB = 0;
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for getPropuestasDeUSuario: "+ duration+"ms");
				    	toastr.clear($(x));
						for (var i=0;i<data.rows.length;i++){  //Need to convert number of votes from string to int (IES API always returns strings)
							data.rows[i].votosAFavor = parseInt(data.rows[i].votosAFavor);
							data.rows[i].votosEnContra = parseInt(data.rows[i].votosEnContra);
							data.rows[i].start = new Date(data.rows[i].start); //Need to convert to JS date object for angularJS filter to work
							data.rows[i].end = new Date(data.rows[i].end);
							data.rows[i].startAportacion = new Date(data.rows[i].startAportacion);
							data.rows[i].endAportacion = new Date(data.rows[i].endAportacion);
							data.rows[i].creationDate = new Date(data.rows[i].creationDate);
							if (data.rows[i].vote==='positivo'||data.rows[i].vote==='negativo'){
								data.rows[i].votoUsuario = data.rows[i].vote;
								delete data.rows[i].vote;
							}
							else {delete data.rows[i].vote;}
						}			
						console.log("getPropuestasDeUSuario() - OK");
						callback(data);
					}).error(function(data) {
						toastr.clear($(x));	
						console.log("getPropuestasDeUSuario() - ERROR");
						if (retryCounterB < 2){  // retry 2 times if original attempt fails
							retryCounterB = retryCounterB + 1;
							$rootScope.updateMyPolls();
						}
						else {						
							
							toastr.error("Unable to retrieve polls. Please try again later.");
							console.log(JSON.stringify("URL: "+uri+" ------  Query: "+MySQLquery+" ------ Response: "+data));
							//callback(data);
						}
					});
				},		
				
				// Gets Aportaciones de propuesta (BRISTOL)
				getAportacionesDePropuesta : function(idPoll,callback) {
					console.log("votingService.getAportacionesDePropuestas()");	
					var MySQLquery = 'SELECT * FROM comments WHERE proposalID = "'+idPoll+'"';
					var uri = "https://iescities.com:443/IESCities/api/data/query/328/sql?origin=original";
					var starttime = (new Date).getTime();
					$http({
						method : 'POST',
						url : uri,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain'
						},
						data: MySQLquery
					}).success(function(data) {
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for getPropuestasDeUSuario: "+ duration+"ms");
						for (var i=0;i<data.rows.length;i++){  //Need to convert number of votes from string to int (IES API always returns strings)
							data.rows[i].votosAFavor = parseInt(data.rows[i].votosAFavor);
							data.rows[i].votosEnContra = parseInt(data.rows[i].votosEnContra);
							data.rows[i].creationDate = new Date(data.rows[i].creationDate); //Need to convert to JS date object for angularJS filter to work
						}		
						console.log("getPropuestasDeUSuario() - OK");
						callback(data);
					}).error(function(data) {
						console.log("getPropuestasDeUSuario() - ERROR");
						toastr.error("Unable to retrieve comments. Please try again later.");
						callback(data);
					});
				},
				
				// Gets Aportaciones de propuesta
				/*getAportacionesDePropuesta : function(idPoll,callback) {
					console.log("votingService.getAportacionesDePropuestas()");				
					return $http.get('http://www.zaragoza.es/api/recurso/gobierno-abierto/propuesta/'+idPoll+'/aportacion.json').success(callback);

				},*/							
				
				// Gets Aportaciones de propuesta con voto de usuario (BRISTOL)
				getAportacionesDePropuestaUsuario : function(idPoll,accountId,callback) {
					console.log("votingService.getAportacionesDePropuestasUsuario()votingService.getAportacionesDePropuestasUsuario()");					
					var uri = "https://iescities.com:443/IESCities/api/data/query/328/sql?origin=original";						
					var MySQLquery = 'SELECT comments.id, comments.comment, comments.username, comments.votosAFavor, '+ 
					'comments.votosEnContra, comments.creationDate, votes.vote FROM comments '+
					'LEFT JOIN votes ON comments.id=votes.commentID AND votes.userID="'+accountId+'" WHERE comments.proposalID='+idPoll;
					var starttime = (new Date).getTime();
					$http({
						method : 'POST',
						url : uri,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain'
						},
						data: MySQLquery
					}).success(function(data) {
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for getAportacionesDePropuestaUsuario: "+ duration+"ms");
						for (var i=0;i<data.rows.length;i++){  //Need to convert number of votes from string to int (IES API always returns strings)
							data.rows[i].votosAFavor = parseInt(data.rows[i].votosAFavor);
							data.rows[i].votosEnContra = parseInt(data.rows[i].votosEnContra);
							data.rows[i].creationDate = new Date(data.rows[i].creationDate); 
							if (data.rows[i].vote==='positivo'||data.rows[i].vote==='negativo'){
								data.rows[i].votoUsuario = data.rows[i].vote;
								delete data.rows[i].vote;
							}
							else {delete data.rows[i].vote;}
						}
						console.log("getAportacionesDeUSuario() - OK");
						callback(data);
					}).error(function(data) {
						alert(JSON.stringify(data));
						console.log("getAportacionesDeUSuario() - ERROR");
						callback(data);
					});
				},
								
				// Create new Poll (STEF: STILL NEEDS WORK ON DATES/TIMES!!!!!!)
				createPoll: function(accountId,poll) {
					console.log("createPoll() - Voting Service: "+JSON.stringify(poll));				
					var start = new Date().toISOString().slice(0, 19).replace('T', ' ');
					var temp = new Date();
					temp.setDate(temp.getDate() + 30);// set end date for 30 days after creation
					var end = temp.toISOString().slice(0, 19).replace('T', ' ');
					var startAportacion = start;
					var endAportacion = end;
					var creationDate = start;
					
					/* Workaround for multi-tier users: if accountId belongs to council/NP, then new polls have type='Council' and category='Plan Director'.
					 * Else, type='Citizen' and category='Propuesta'*/
					var type = "Citizen";
					var category = "Propuesta";
					if (accountId==0||accountId==2768){  //2768 is username 'youdecide' and their polls go into 'Council Polls'
						console.log("This user is a council person!");
						type = "Council";
						category = "Plan Director";
					}
					
					if (accountId==1||accountId==2870){ //2870 is username 'youdecideNM' and their polls go into 'Neighbourhood Choices'
						console.log("This user is a neighbourhood manager!");
						type = "Council";
						category = "Otros";
					}
					
					
					var MySQLquery ='INSERT INTO proposals (title, type, category, description, aportaciones, votosAFavor, '+
					'votosEnContra, start, end, startAportacion, endAportacion, creationDate, userID) '+
					'VALUES ("'+poll.title+'", "'+type+'", "'+category+'", "'+ poll.description+'", "true", 0, 0, "'+
					start+'", "'+end+'", "'+startAportacion+'", "'+endAportacion+'", "'+creationDate+'", '+accountId+')';
					var starttime = (new Date).getTime();
					return $http({
						method : 'POST',
						url : 'https://iescities.com:443/IESCities/api/data/update/328/sql?transaction=false',
						data : MySQLquery,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain',
							'Authorization': 'Basic ' + btoa("bris_democratree:83x6m75tP2Ms")
						}
					}).success(function(data) {
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for createPoll: "+ duration+"ms");
						Restlogging.appLog(logType.COLLABORATE,"new poll added");										
						$rootScope.updateMyPolls();
						/*update polls with users votes*/
						$rootScope.updatePolls();
						$rootScope.showPanel("pollList");
					}).error(function(data,status) {
						 console.log("Error:" + data.error + status.error +"  "+ JSON.stringify(data));
					});
				},
				
				// Create new Aportacion (BRISTOL)
				createAportacion: function(accountId,pollId,aportacion,callback) {					
					var creationDate = '2015-10-21 17:00';
					var MySQLquery = 'INSERT INTO comments (proposalID, userID, username, votosAFavor, votosEnContra, creationDate, comment) '+
					'VALUES ("'+pollId+'", "'+accountId+'", "'+$rootScope.profileData.name+'", 0, 0, "'+creationDate+'", "'+aportacion.comment+'")';
					var starttime = (new Date).getTime();
					//var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					return $http({
						method : 'POST',
						url : 'https://iescities.com:443/IESCities/api/data/update/328/sql?transaction=false',
						data : MySQLquery,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain',
							'Authorization': 'Basic ' + btoa("bris_democratree:83x6m75tP2Ms")
						}
					}).success(function(data) {
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for createAportacion: "+ duration+"ms");
				    	//toastr.clear($(x));
						Restlogging.appLog(logType.COLLABORATE,"new contribution added");						
						console.log("Comment created");
						toastr.info("Comment saved", 'Thank you', {timeOut: 3000, closeButton : true});
						callback(data);
						$rootScope.updateMyPolls();
						$rootScope.updatePolls();
						
						$("#newaportacion").hide();
				    	$("#shownewaportacion").show();
				    	$("#sendaportacion").hide();
				    	$rootScope.calculateData($rootScope.selectedPoll);
					}).error(function(status,data,error) {
						console.log("Error al crear aportación:" + JSON.stringify(error));
						//toastr.clear($(x));
						toastr.error("Unable to save comment at this moment. Please try again later.");
					});
				},			
				
				
				// Vote Poll (BRISTOL)
				votePoll : function(pollId,voto,accountId) {
					console.log("URI:"+ pollId+" "+voto+" "+accountId);
					var uri = "https://iescities.com:443/IESCities/api/data/update/328/sql?transaction=true";
					///// STEF: NEED TO UPDATE VOTES TABLE AS WELL!!!!!!
					var MySQLquery = "";
					var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					if (voto=='a-favor'){
						MySQLquery = 'UPDATE proposals SET votosAFavor = votosAFavor + 1 WHERE id='+pollId+';';
						MySQLquery = MySQLquery + 'INSERT INTO votes (proposalID, vote, userID) VALUES('+pollId+',"positivo",'+accountId+');';
					}
					else{
						MySQLquery = 'UPDATE proposals SET votosEnContra = votosEnContra + 1 WHERE id='+pollId+';';
						MySQLquery = MySQLquery + 'INSERT INTO votes (proposalID, vote, userID) VALUES('+pollId+',"negativo",'+accountId+');';
					}
					var starttime = (new Date).getTime();
					return $http({
						method : 'POST',
						url : uri,
						data: MySQLquery,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain',
							'Authorization': 'Basic ' + btoa("bris_democratree:83x6m75tP2Ms")
						}
					}).success(function(data) {
						toastr.clear($(x));
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for votePoll: "+ duration+"ms");
						console.log("vote OK");
						Restlogging.appLog(logType.COLLABORATE,"poll voted");						
					}).error(function(data) {
						toastr.clear($(x));
						console.log("vote Error");
						alert(JSON.stringify(data));
					});
				},
				
				
				// Vote Aportacion (BRISTOL)
				voteAportacion : function(aportacionId,voto,accountId) {										
					var uri = "https://iescities.com:443/IESCities/api/data/update/328/sql?transaction=true";
					var MySQLquery = "";
					if (voto=='a-favor'){
						MySQLquery = 'UPDATE comments SET votosAFavor = votosAFavor + 1 WHERE id='+aportacionId+';';
						MySQLquery = MySQLquery + 'INSERT INTO votes (commentID, vote, userID) VALUES('+aportacionId+',"positivo",'+accountId+');';
					}
					else{
						MySQLquery = 'UPDATE comments SET votosEnContra = votosEnContra + 1 WHERE id='+aportacionId+';';
						MySQLquery = MySQLquery + 'INSERT INTO votes (commentID, vote, userID) VALUES('+aportacionId+',"negativo",'+accountId+');';
					}
					var starttime = (new Date).getTime();
					var x = toastr.info('Updating data...','', {timeOut: 50000, closeButton : false});
					return $http({
						method : 'POST',
						url : uri,
						data: MySQLquery,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'text/plain',
							'Authorization': 'Basic ' + btoa("bris_democratree:83x6m75tP2Ms")
						}
					}).success(function(data) {
						toastr.clear($(x));
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for voteAportacion: "+ duration+"ms");
						console.log("vote OK");
						Restlogging.appLog(logType.COLLABORATE,"contribution voted");
						$rootScope.updateMyPolls();
						/*update polls with users votes*/
						$rootScope.updatePolls();
						$rootScope.showPanel("pollList");
						
					}).error(function(data,error,status) {
						toastr.clear($(x));
						console.log("status:"+JSON.stringify(status)+"error:"+JSON.stringify(error)+"data"+JSON.stringify(data));
					});
				}
				
			};
		} ]);


/** Login service (BRISTOL) */
/** User Login service using the IES Cities API for user management**/
iescitiesAppServices.factory('loginService', [
		'$http',
		'$rootScope',		
		function($http, $rootScope) {
			return {
				// Starts login
				login : function(userData, callback) {					
					console.log("login() - loginService "+userData.email+" "+userData.password);
					var starttime = (new Date).getTime();
					return $http({
						method : 'GET',
						url : 'https://iescities.com:443/IESCities/api/entities/users/login',
						data : "",
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'application/json',
							'Authorization': 'Basic ' + btoa(userData.email+":"+userData.password)
							//'Authorization': 'Basic ' + btoa("ghostaki:vatsikas")
						}
					}).success(function(data , status, header, config ) {
						var duration = (new Date).getTime() - starttime;
				    	console.log("==> Time for login: "+ duration+"ms");
						//transform json object to the form expected by the app (as dictated by the ZGZ API)
						var data2 = { 
							    "name" : data.name,
							    "email" : data.email,
							    "account_id" : data.userId,
							    "password" : userData.password,
							};
						$rootScope.accountId = data2.account_id;
						$rootScope.profileData = data2;
						$rootScope.profileData.password = data2.password;  //Not sure why this fails, but de-activated it. Maybe will cause issues elsewhere
						$rootScope.logged = true;
						Restlogging.appLog(logType.CONSUME, "user logged");		
						var x = toastr.info("Thank you for logging in "+data2.name+"!",'',{timeout:3000});
						setTimeout(function () {
							toastr.clear($(x));
							callback(data2)
					    },2000);
						
						;
					}).error(function(data, status, header, config) {
						console.log("Error:"+JSON.stringify(data)+" Status:"+status);
						toastr.error("Login failed!");
					});
				}
			};
		}]
	);
		
		

/** Profile info service */
iescitiesAppServices.factory('profileInfoService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {
				/* Get profile info*/
				getProfileInfo : function(callback) {
					return $http.get('scripts/iescities/data/getProfileInfo.json').success(callback);
				},
				
				
				/* Create new user */
				createNewUser : function(profile, schoolType , callback) {
					//alert(JSON.stringify(profile));
					var user = new Object();
					user.name = profile.name;
					user.password = profile.password;
					user.username = profile.username;
					user.email = profile.email;
					user.preferredCouncilId = "Bristol";
					user.profile="User";
					var params = JSON.stringify(user);
					
					return $http({
						method : 'POST',
						url : 'https://iescities.com:8443/IESCities/api/entities/users',
						data : params,
						headers : {
							'Accept' : 'application/json',
							'Content-Type' : 'application/json'
						}
					}).success(function(data, status, header, config) {
						console.log("Response: " + JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						
				
						
					}).error(function(data) {
						console.log("Response: " + JSON.stringify(data));
					});
				}
			};
		} ]);