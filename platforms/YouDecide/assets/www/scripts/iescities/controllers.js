'use strict';

/* Controllers */
var iescitiesAppControllers = angular.module('iescitiesAppControllers', []);

/* Main controller */
// FIXME: Sacar controlador de main.html y crear los necesarios
iescitiesAppControllers.controller('MainCtrl', ['$rootScope', '$scope',
	function($rootScope, $scope) {
	} 
]);

/* Menu controller */
iescitiesAppControllers.controller('menuCtrl', ['$rootScope', '$scope', 'votingService',
	function($rootScope, $scope, votingService) {
		// Category description for poll list header. This is the correct value at startup
		$rootScope.category = "All";
	
	
		// Close panel
		$rootScope.closePanel = function(panelId) {			
			if (isWebKit()) {
				document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
			} 
			else 
			{
				document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
			}
			document.getElementById(panelId).style.zIndex = 999;
		
			if (panelId == 'details'){		
				//Content detail size
				$rootScope.emSizeContenido = 14.2 * parseFloat($("body").css("font-size"));
				$("#details .contenido").height("100%").height("-="+$rootScope.emSizeContenido+"px");
				$rootScope.closeVoteOption();
				$rootScope.optionSelected = null;
			}
		};	
	
		// Close all panels
		$rootScope.closeAllPanels = function() {
			$rootScope.closePanel('mypolls');
			$rootScope.closePanel('categories');
			$rootScope.closePanel('results');
			$rootScope.closePanel('details');
			$rootScope.closePanel('pollList');
			$rootScope.closePanel('login');
			$rootScope.closePanel('newpoll');
			$rootScope.closePanel('about');
		};			
	
		// Show panel with panelId
		$rootScope.showPanel = function(panelId) {
			if (panelId != "new_user"){ // en Nuevo Usuario no cerrar panel
				$rootScope.closeAllPanels();
			}
			if (isWebKit()) {
				document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
			} 
			else 
			{
				document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
			}
			document.getElementById(panelId).style.zIndex = 200;
			hideKeyboard();
		};
	
		// Show results
		$rootScope.showResults = function(poll) {
			$rootScope.optionSelected = "aaaa";
			$rootScope.calculateData(poll);
			$rootScope.showPanel('results');
		};
	
		// Calculate poll data
		$rootScope.calculateData = function(poll) {		
			$rootScope.selectedPoll = poll;			
			$rootScope.aportaciones=[];
			$rootScope.labels = [];
			$rootScope.data = [];
			if (poll.votosAFavor ==null){
				$rootScope.selectedPoll.votosAFavor = 0;
			}
			if (poll.votosEnContra ==null){
				$rootScope.selectedPoll.votosEnContra = 0;
			}				
			if (!$rootScope.logged) {
				votingService.getAportacionesDePropuesta(poll.id,function(data) { 
					console.log("Aportaciones:"+data.count);
					$rootScope.aportaciones = data.rows;
				});
			} 
			else 
			{
				votingService.getAportacionesDePropuestaUsuario(poll.id, $rootScope.accountId,function(data) { 
					console.log("Aportaciones usuario:"+data.count);
					$rootScope.aportaciones = data.rows;
				});
			}		
			$rootScope.totalVotos = $rootScope.selectedPoll.votosAFavor+ $rootScope.selectedPoll.votosEnContra;
			if ($rootScope.totalVotos != 0){
				$rootScope.porcentajeAFavor = Math.round(poll.votosAFavor*100/$rootScope.totalVotos);
				$rootScope.porcentajeEnContra = Math.round(poll.votosEnContra*100/$rootScope.totalVotos);
				
			}
			else 
			{
				$rootScope.porcentajeAFavor = 0;
				$rootScope.porcentajeEnContra = 0;
			}		
			$rootScope.data.push($rootScope.selectedPoll.votosAFavor);
			$rootScope.data.push($rootScope.selectedPoll.votosEnContra);
			$rootScope.labels.push("I agree");
			$rootScope.labels.push("I disagree");
		};
			
		// Show poll details
		$rootScope.showDetails = function(poll) {
			$rootScope.optionSelected = null;
			$rootScope.calculateData(poll);
			$rootScope.showPanel('details');			
			$rootScope.marcarVoto();
		};
	
		// Mark vote in the detail screen
		$rootScope.marcarVoto = function() {			
			console.log("votoPoll = "+ $rootScope.selectedPoll.votoUsuario);			
			if ($rootScope.selectedPoll.votoUsuario){
				if ($rootScope.selectedPoll.votoUsuario == "positivo"){
					$(".opcionSelec").html("I agree");
				}
				else if ($rootScope.selectedPoll.votoUsuario == "negativo")
				{
					$(".opcionSelec").html("I disagree");
				}
				else 
				{
					$(".opcionSelec").html($rootScope.selectedPoll.votoUsuario);
				}
			}
			else 
			{
				$(".opcionSelec").html("");
			} 
		};	
	
		// Show poll options
		$rootScope.showPollopt = function(poll,index) {
			$(".votar").removeClass("slideIn");
			$("#indexpollvotar_"+index).addClass("slideIn");
		};
	
		// Handle categories
		$scope.contarCategorias= function() {
			$rootScope.encuestasPropuesta = [];
			$rootScope.encuestasPlan = [];
			$rootScope.encuestasEncuesta = [];
			$rootScope.encuestasOtros = [];
			$rootScope.encuestasTotales = [];
			jQuery.each($rootScope.encuestas, function(i, val) {
				if (val.category == "Propuesta"){
					$rootScope.numPropuestas =  $rootScope.numPropuestas +1;
					$rootScope.encuestasPropuesta.push(val);
				}
				else if (val.category == "Plan Director")
				{
					$rootScope.numPlanes =  $rootScope.numPlanes +1;
					$rootScope.encuestasPlan.push(val);
				}
				else if (val.category == "Encuesta Ciudadana")
				{
					$rootScope.numEncuestas =  $rootScope.numEncuestas +1;
					$rootScope.encuestasEncuesta.push(val);
				}
				else
				{
					$rootScope.numOtros =  $rootScope.numOtros +1;
					$rootScope.encuestasOtros.push(val);
				}
			});
			$rootScope.encuestasTotales = $rootScope.encuestas;
		};
	
		// Update polls
		$rootScope.updatePolls = function() {
			$rootScope.numPropuestas = 0;
			$rootScope.numPlanes = 0;
			$rootScope.numEncuestas = 0;
			$rootScope.numOtros = 0;			
			if ($rootScope.logged) {
				console.log("LOGGED");
				votingService.getPropuestasConVoto($rootScope.accountId,function(data) { 							
					$rootScope.encuestas = data.rows;
					$scope.contarCategorias();	
				});
			} 
			else 
			{
				console.log("NO LOGGED");
				votingService.getPropuestas(function(data) { 
					$rootScope.encuestas = data.rows;
					$scope.contarCategorias();
				});
			}				
			$(".votar").addClass("slideOut");
		};
	
		// Filter by category
		$rootScope.filterByCategory = function(category) {	};
	
		// Click menu icon
		$rootScope.clickMenuIcon = function(){
			 $("#menu").addClass("open");
		};
	
		// Click menu
		$rootScope.clickMenu = function(){		
			 $("#menu").removeClass("open");
		};
	
		// Update my polls
		$rootScope.updateMyPolls = function(){		
			votingService.getPropuestasDeUsuario($rootScope.accountId,function(data) { 
				console.log("Número mis propuestas:"+data.rows.length);
				$rootScope.mypolls = data.rows;
			});
			$(".votar").addClass("slideOut");	
		};
		
		// Logged and accountId initialization 
		$rootScope.logged = false;
		$rootScope.accountId = "";

	
		// Polls initialization
		$rootScope.encuestas= [];
		$rootScope.updatePolls();
	
		// Size of poll list
		$rootScope.alturaHeader = $("#pollList header").height();
		$("#pollList .lista_poll").height("100%").height("-="+$rootScope.alturaHeader+"px");
		
	} 
]);


/* Results controller */
iescitiesAppControllers.controller('resultsCtrl', [ '$rootScope', '$scope',  'votingService',
	function($rootScope, $scope, votingService) {
	
		$rootScope.optionSelected = null;	
		
		// parameters for setOption animation (Results controller)		  
		$rootScope.voteOptionPosition = $(".voteOption").position();
	
		// Vote comment/aportacion
		$scope.voteAportacion = function (aportacionId,voto) {
			  console.log("Vote Aportacion: "+aportacionId+" "+voto);
			  console.log("from controllers line 261: Is user logged in? "+JSON.stringify($rootScope.logged));
			  if ($rootScope.logged==false){
				  toastr.info("Please login before voting");
			  }
			  else 
			  {
				  votingService.voteAportacion(aportacionId,voto, $rootScope.accountId)
				  	.success(
				  			function(data) {
				  				// Actualizar aportaciones			
				  				votingService.getAportacionesDePropuestaUsuario($rootScope.selectedPoll.id, $rootScope.accountId,function(data) { 
				  					console.log("Aportaciones JSON: "+JSON.stringify(data));
				  					$rootScope.aportaciones = data.rows;
				  				});						
				  				toastr.info("Your vote has been saved. Thank you for voting!");
				  			})
				  	.error(
				  			function(data) {
				  				toastr.error("Unable to save vote at this moment. Please try again later.");
		    			    });
			  }
		};
	  
		// Create comment/aportacion
		$scope.createAportacion = function () {
		  	console.log("Crear Aportacion: "+$scope.newaportacion.comment);
		  	votingService.createAportacion($rootScope.accountId,$rootScope.selectedPoll.id,$scope.newaportacion,function(){
		  		$("#newaportacion").hide();
		  		$("#shownewaportacion").show();
		  		$("#sendaportacion").hide();
		  		$rootScope.showPanel('details');	
	    	});		    	
		    	  
		};
	
		// Display toastr message
		$scope.message = function(msg){
			toastr.info(msg);
		};
		  
		// Show new comment/aportacion
		$scope.showNewAportacion = function () {
			if (!$rootScope.logged){
				toastr.info("Please login before submitting a comment.");
			}
			else 
			{
				console.log("show Aportacion: ");
				$("#newaportacion").show();
				$("#shownewaportacion").hide();
				$("#sendaportacion").show();
			} 
		};
	  
		// Cancel new comment
		$scope.cancelAportacion = function () {
			console.log("cancel comment");
			$("#newaportacion").hide();
			$("#shownewaportacion").show();
			$("#sendaportacion").hide(); 
		};
	 
		// Show details
		$scope.showDetails = function () {
			$rootScope.optionSelected = null; 
			$rootScope.marcarVoto();
			$rootScope.showPanel('details');
		};
	  
		// Handle tap event
		$scope.tapEvent = function() {
			$(".voteOption").height("auto");
			$(".voteOption").css("top", top);
			// crearTapEvent();
			$(".voteOption").animate({top: "9em", bottom: "0", height: "auto"}, 300, function () {
				// Animation complete.
			}).css("background-image", "none");
			$("#vota").hide();
			$(".opcionSelec").hide();
			$(".encuestaDetalle").hide();
			$(".encuestaOpciones").show();
		};
	  
		// Show option
		$scope.showOption = function (option) {	   
			console.log("showOPtion()"+option.votes+"/"+$rootScope.selectedPoll.votes);
	    	$scope.showOption=option;
	    	$scope.percentage= Math.round(option.votes *100/ $rootScope.selectedPoll.votes);
		};
	  
		// Show results
		$scope.showResults = function () {	  
			$rootScope.optionSelected = null; 
			$rootScope.showResults($rootScope.selectedPoll);
		};
	  
		// Set option
		$scope.setOption = function (option) {
			console.log("Selected Option:"+ option);
			$rootScope.optionSelected = option;
		/* Translate from spanish to english for display on the vote/result page */
			if (option=='a-favor'){
				option = 'I agree';
			}
			else 
			{
				option = 'I disagree';
			}  
			$(".opcionSelec").html(option);      	 
			$scope.closeVoteOption();  	    
		};
	  
		// Close vote option
		$rootScope.closeVoteOption = function () {
			$(".voteOption").animate({top: $rootScope.voteOptionPosition.top, bottom: "4em", height:  "6em"}, 300, function () {
			// Animation complete.
			}).css("background-image", "url(img/fondo_voteOption.png)");
			$(".encuestaDetalle").show();
			$("#vota").show()
			$(".encuestaOpciones").hide();
			$(".botonera").show();
			$(".opcionSelec").show();  
		};
	   
		// Set vote
		$scope.setVote = function () {
			console.log("Voting for :"+ $rootScope.selectedPoll.id+" "+ $rootScope.optionSelected);		  
			votingService.votePoll($rootScope.selectedPoll.id, $rootScope.optionSelected,$rootScope.accountId)
				.success(
						function(data) {
							$rootScope.selectedPoll.votoUsuario = $rootScope.optionSelected;
							if ($rootScope.optionSelected === "a-favor"){
								$rootScope.selectedPoll.votosAFavor++;
							}
							else
							{
								$rootScope.selectedPoll.votosEnContra++;
							}
							console.log("VOTO === "+$rootScope.optionSelected);
							console.log(JSON.stringify($rootScope.selectedPoll));
							$rootScope.optionSelected = null; 
							$rootScope.calculateData($rootScope.selectedPoll);
				})
				.error(
					function(data) {
						toastr.error("Unable to save vote, please try again later.");
						console.log("VOTO === "+$rootScope.optionSelected);
				});
		};
	  	  
	  $rootScope.notLoggedIn = function () { toastr.info("Please login before voting");};	  
	  $rootScope.alreadyVoted = function () { toastr.info("You have already voted for this poll");}	  
	  $rootScope.noSelection = function () { toastr.info("Please select an option first");}  			
	} 
]);

/* Categories controller */
iescitiesAppControllers.controller('categoriesCtrl', [ '$rootScope', '$scope', 'votingService',
	function($rootScope, $scope, votingService) {		
		$scope.categories= [];		
		votingService.getCategories(function(data) { 
			console.log("Categories" +data);
			$scope.categories = data;
		});		
		$scope.viewPolls = function(category){
			console.log("Show Polls filtered by:" +category);			
			if (category == "propuesta"){
				$rootScope.category = "Your ideas";
				$rootScope.encuestas = $rootScope.encuestasPropuesta;
			}
			else if (category == "plan")
			{
				$rootScope.encuestas = $rootScope.encuestasPlan;
				$rootScope.category = "Council";			 
			}	
			else if (category == "encuesta")
			{
				$rootScope.encuestas = $rootScope.encuestasEncuesta;
				$rootScope.category = "Surveys";
			}
			else if (category == "otros")
			{
				$rootScope.encuestas = $rootScope.encuestasOtros;		
				$rootScope.category = "Neighbourhood";
			}
			$rootScope.showPanel("pollList");
		};
	} 
]);

/* NewPoll controller */
iescitiesAppControllers.controller('newpollCtrl', [ '$rootScope', '$scope',  'votingService',
	function($rootScope, $scope,  votingService) {
		$scope.newpoll = null;
		$scope.createPoll = function(){
			if ($scope.newpollForm.$valid) {
				console.log("Creating Poll:" +$scope.newpoll.title);
				votingService.createPoll($scope.accountId,$scope.newpoll);
				$rootScope.showPanel("pollList");
				$scope.newpoll.title = "";
				$scope.newpoll.description = "";
			} 
			else 
			{
				if ($scope.newpollForm.title.$invalid){
					toastr.error("The poll title has to be between 10 and 200 characters");
				}
				else 
				{
					toastr.error("The poll description has to be between 10 and 500 characters");
				}
			}	
		};			
	} 
]);

/* Side Nav controller */
iescitiesAppControllers.controller('navCtrl', [ '$rootScope', '$scope', 
	function($rootScope, $scope) {
		$scope.openCategories = function(){
			$rootScope.showPanel('categories');
		};		
		$scope.openMypolls = function(){
			$rootScope.showPanel('mypolls');
		};		
		$scope.openLogin = function(){
			$rootScope.showPanel('login');
		};		
		$scope.openList = function(){
			$rootScope.category = "All";
			$rootScope.encuestas = $rootScope.encuestasTotales;
			$rootScope.showPanel('pollList');
		};
		$scope.openNewpoll = function(){
			$rootScope.showPanel('newpoll');
		};	
		$scope.openIESSurvey = function(){
			startRatingSurvey(true);
		};
		$scope.openAbout = function(){
			$rootScope.showPanel('about');
		};
		$scope.openTC = function(){
			viewTC();
		};
	} 
]);

/* My Polls controller */
iescitiesAppControllers.controller('mypollsCtrl', [ '$rootScope', '$scope',  'votingService',
	function($rootScope, $scope, votingService) {
		$rootScope.mypolls = [];
		$rootScope.mypolls = $rootScope.updateMyPolls();
	    $rootScope.showMyPollopt = function(poll,index) {
			$(".votar").removeClass("slideIn");
			$("#indexmypollvotar_"+index).addClass("slideIn");
			console.log("show slideIn "+index);
		};
	}
]);

/* Profile controller */
iescitiesAppControllers.controller('profileCtrl', ['$rootScope', '$scope', '$http', 'profileInfoService',                                             
    function($rootScope, $scope, $http, profileInfoService) {	
		// Close panel
		$scope.closePanel = function(panelId) {
			if (isWebKit()) {
				document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
			} 
			else 
			{
				document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
			}
			document.getElementById(panelId).style.zIndex = 999;
		};
	
		// Creates new user
		$scope.createNewUser = function() {						
			if ($scope.profileData == null) {
				toastr.info("Name, email, username and password are compulsory");
				console.log($scope.profileData);
				return;
			}	
			if ($scope.profileData.name == null) {
				toastr.info("Please fill in your name");
				return;
			}	
			if ($scope.profileData.email == null) {
				toastr.info("Please fill in your email address");
				return;
			}
			if ($scope.profileData.password == null) {
				toastr.info("Please enter a password");
				return;
			}
			if ($scope.profileData.passwordConfirm == null) {
				toastr.info("Please confirm the password");
				return;
			}
			if ($scope.profileData.password != $scope.profileData.passwordConfirm) {
				toastr.info("The passwords do not match");
				return;
			}
			var profileBody = {
				"name" : $scope.profileData.name,
				"email" : $scope.profileData.email,
				"username" : $scope.profileData.username,
				"password" : $scope.profileData.password
			};								
			console.log(profileBody);						
			profileInfoService.createNewUser(profileBody)
				.success(
						function(data) {
							toastr.info("Registration complete");
							$scope.closePanel('new_user');
						})
				.error(
						function(data) {
							toastr.error("Registration error: "+ data.message);
						});							
			};
	
		$scope.closePanelWithId = function(panelId) {
			$scope.closePanel(panelId);
		};
	} 
]);


/** Login controller */
iescitiesAppControllers.controller('loginCtrl', ['$scope', '$rootScope', '$http', 'loginService', 'votingService',
	function($scope, $rootScope, $http, loginService, votingService) {
	
		// Close panel
		$scope.closePanel = function(panelId) {
			if (isWebKit()) {
				document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
		} else 
		{
			document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
		}
			document.getElementById(panelId).style.zIndex = 999;
		};
		
		$scope.rememberLoginData = window.localStorage.getItem("rememberLoginData") == 'true' ? true: false;
		
		// Load stored account credential values
		$scope.loginData = {
			email : window.localStorage.getItem("email"),
			password : window.localStorage.getItem("password")
		};
		
		$scope.userLogin = function() {						
			loginService.login($scope.loginData,function(data){
				console.log(JSON.stringify(data));
				$rootScope.updateMyPolls(); //load my proposals
				$rootScope.updatePolls(); //update polls with users votes
				$rootScope.showPanel("pollList");	
			});							
		};

		$scope.openNewUserPanel = function() {
			$scope.showPanel('new_user');
		}
		
		$scope.closePanelWithId = function(panelId) {
			$scope.closePanel(panelId);
		};
	}]
);

/* Controller - Privacy - Privacy policy */
iescitiesAppControllers.controller('privacyCtrl', ['$rootScope', '$scope',
	function($rootScope, $scope) {	
		$rootScope.privacyVisible = false;	
		var isPrivacyAccepted = window.localStorage.getItem("isPrivacyAccepted");
		if (isPrivacyAccepted) {
			$rootScope.privacyVisible = false;
		} 
		else 
		{
			$rootScope.privacyVisible = true;
		}	
		$scope.acceptPrivacy = function() {
			$rootScope.privacyVisible = false;
			window.localStorage.setItem("isPrivacyAccepted", true);
		};
		$scope.refusePrivacy = function() {
			navigator.app.exitApp();
		};
	} 
]);

/* Chart controller */
iescitiesAppControllers.controller("DoughnutCtrl", function ($scope) {
	$scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
	$scope.data = [300, 500, 100];
});